
// Copyright (c) 2002-2021, Hard & Soft Technology Co.,LTD.
// SPDX-License-Identifier: Apache-2.0
// https://gitee.com/hsav20/kc3xm51.git
// https://github.com/hsav20/kc3xm51.git
// http://www.hsav.com/download/kc3xm51.zip

#include "main.h"           

void MAUD_Initialize(){										// 按键模块初始化  	
}	  
void MAUD_10msTimer(BYTE baseTimer){   						// B3=1000ms B2=500ms B1=100ms B0=10ms 
    if (gRemoveTimer && ++gRemoveTimer > 200){			    // 大约2秒后 
		INPUT_SWITCH select;
		BYTE value = KcmGetRegister(KCM_EXTR_MEMORY + MEM_SOURCE_AUTO);	// 自动输入的恢复
    	select = MKCM_FromRegister(KCM_INPUT_SOURCE, value);				// 从KCM来的寄存器，转换到本机处理的值
		MLOG("RemoveA %02x %d ", value, select,mINPUT_SWITCH);
		gDIP_MenuSelect = MENU_RESTORE;				        // 菜单即刻进入输入的恢复 
		MAUD_InputSelect(select);
    	gRemoveTimer = 0;
    }
}

void MAUD_MixMasterVolume(BYTE directUp){
	BYTE value = KcmGetRegister(KCM_VOLUME_CTRL);			// 读取原来的音量值
	MAUD_AutoCanclTestTone();
	if (directUp){											// 加大音量
		//BYTE max = KcmGetRegister(KCM_VOLUME_MAX);			// 读取音量最大值
		//if (value < max){
		if(value < MAX_STEP_VOLUME){						// 小于最大值
			KcmSetRegister(KCM_VOLUME_CTRL, value + 1);		// 改变音量值
		}
        if (FSYS_MuteEnable){								// 原来是在静音状态		   
			BYTE flag = KcmGetRegister(KCM_FLAG_CTRL);
			FSYS_MuteEnable = 0;
			KcmSetRegister(KCM_FLAG_CTRL, flag & ~FLAG_CTRL_MUTE);	  // 静音关闭 
        }
	}else {													// 减小音量
		if (value > 0){										// 不是最小音量
			KcmSetRegister(KCM_VOLUME_CTRL, value - 1);		// 改变音量值
		}
	}
}
void MAUD_TestToneChannel(BYTE channel){					// 噪音测试
	if (channel < 0xff){
		KcmSetRegister(KCM_TEST_TONE, 0x10 | MKCM_ToRegister(KCM_TEST_TONE, channel));	// 噪音测试通道
//MDEBUG(0xb1);MDEBUG(0x10 | MKCM_ToRegister(KCM_TEST_TONE, channel));
	}
	else {									 				// 关闭
		KcmSetRegister(KCM_TEST_TONE, 0);				// 噪音测试关闭
	}
}
/*
void MAUD_MixSoundEffect(){
	MAUD_AutoCanclMute();
	MAUD_AutoCanclTestTone();
	if (++gDIP_SoundEffect > 4){
		gDIP_SoundEffect = 0;
	}
	KcmSetRegister(KCM_EQ_SELECT, gDIP_SoundEffect);	// 选择需要的音效处理通道
//MDEBUG(0xa7);MDEBUG(gDIP_SoundEffect);
}
*/
void MAUD_MixTrimAdjust(BYTE index, BYTE directUp){
    BYTE address;
    BYTE value;

	MAUD_AutoCanclMute();
	if (directUp){
		if (gDIP_TrimCtrl[index] < 18){
			++gDIP_TrimCtrl[index];
		}
	}
	else {
		if (gDIP_TrimCtrl[index] > 0){
			--gDIP_TrimCtrl[index];
		}
	}    
    address = MKCM_ToRegister(KCM_TEST_TONE, index);
    value = MKCM_ToRegister(KCM_FL_TRIM, gDIP_TrimCtrl[index]);
//MLOG("MAUD_MixTrimAdjust A %d %d %d", index, KCM_FL_TRIM + address, value);
    KcmSetRegister(KCM_FL_TRIM + address, value);
}
void MAUD_AutoCanclTestTone(){
//MDEBUG(0xa7);
	if (FSYS_TestTone){
		FSYS_TestTone = 0;
		KcmSetRegister(KCM_TEST_TONE, 0);				// 噪音测试关闭
	}
}
void MAUD_AutoCanclMute(){
	if (FSYS_MuteEnable){
		FSYS_MuteEnable = 0;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 音源输入选择及抢占式处理  //////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void MAUD_InputSelect(INPUT_SWITCH select){
    DIP_SURROUND_OFF();
    DIP_SRC_FORMAT_OFF();
	MAUD_AutoCanclMute();
	MAUD_AutoCanclTestTone();	
	if (gDIP_MenuSelect == MENU_RESTORE || gDIP_MenuSelect == MENU_INPUT_SOURCE){	// 只有菜单已经换到MENU_RESTORE后，才可以改变当前的输入
		MLOG("抢占TT:select(%02x) last(%02x)", select,  mINPUT_SWITCH);
		MAUD_InputWrite(select, mINPUT_SWITCH);
		mINPUT_SWITCH = select;
		if (mINPUT_SWITCH == INPUT_SWITCH_SD || mINPUT_SWITCH == INPUT_SWITCH_UDISK){
			MLOG("播放AAAAAA");
			MKCM_PlayIndex(false, false);				   	// SD卡或U盘播放前/后一个文件
		}else{
			MDIP_PlaySymbol(0);								// 灭了SD卡或U盘的显示
		}
	}
	MDIP_MenuNormal(MENU_INPUT_SOURCE);                    	// 菜单状态:输入音源选择
}
void MAUD_Preemptible(){									// 云音乐(切换UDisk或SD卡输入) 抢占式输入选择 
	uint8_t status = KcmGetRegister(KCM_PLAY_STATE);   		// 读取多媒体文件播放状态
	MLOG("抢占BAA:(%d)(%d)(%d)(%d)",gPreemptibleQty, gPreemptibleIn[gPreemptibleStep], mINPUT_SWITCH, gPreemptibleStep);
	if(gPreemptibleQty >= 2){								// 同时有SD卡和U盘的时候
		if(status & KCM_STATE_PLAY_PLAY){					// 播放的状态
			MLOG("抢占KKKK");
			IsChangeDevice = 1;
			KcmSetRegister(KCM_PLAY_OPERATE, KCM_OPERATE_STOP); // 多媒体播放停止
		}else{													// 不是播放的状态
			MDIP_InputTypee(gPreemptibleIn[gPreemptibleStep]);	// 显示输入类型的字眼
			MAUD_InputSelect(gPreemptibleIn[gPreemptibleStep]);
			MLOG("抢占GGA:(%d)(%d)", gPreemptibleStep , gPreemptibleQty);
			if (++gPreemptibleStep >= gPreemptibleQty){
				gPreemptibleStep = 0;
			}		
		}
	}
	
	if(gPreemptibleQty == 1){								// 只有一个设备的时候
		if (!(status & KCM_STATE_PLAY_PLAY)){				// 不是播放的状态
			if (++gPreemptibleStep >= gPreemptibleQty){
				gPreemptibleStep = 0;
			}
			MDIP_InputTypee(gPreemptibleIn[gPreemptibleStep]);	// 显示输入类型的字眼
			MAUD_InputSelect(gPreemptibleIn[gPreemptibleStep]);
		}
	}
}
CONST_CHAR TabInputOneKey[] = {
	INPUT_SWITCH_AUX,                                   	// 0=模拟输入
    INPUT_SWITCH_OPTIC,                                 	// 1=数码1   
    INPUT_SWITCH_COA1,                                  	// 2=数码2   
    INPUT_SWITCH_COA2,                                  	// 3=数码3   
};

    // INPUT_SWITCH_HDMI1 = 9,                                 // HDMI1   
    // INPUT_SWITCH_HDMI2 = 10,                                // HDMI2   
    // INPUT_SWITCH_HDMI3 = 11,                                // HDMI3   
    // INPUT_SWITCH_H_ARC = 12,                                // HDMI-ARC


void MAUD_InputOneKey(){									// 所有输入用一个按键选择 
	gDIP_MenuSelect = MENU_RESTORE;							// 菜单即刻进入输入的恢复 
	if (gWithHdmiQty && gWithHdmiStep < gWithHdmiQty){	 	// 循环方式
		MAUD_InputSelect(gPreemptibleIn[gWithHdmiStep]);
	}else {
		MAUD_InputSelect(TabInputOneKey[gWithHdmiStep - gWithHdmiQty]);
	}
	if (++gWithHdmiStep >= (sizeof(TabInputOneKey) + gWithHdmiQty)){
		gWithHdmiStep = 0;
	}
}
void MAUD_InputWrite(INPUT_SWITCH select, INPUT_SWITCH last){
	BYTE value1 = MKCM_ToRegister(KCM_INPUT_SOURCE, select);
	BYTE value2 = MKCM_ToRegister(KCM_INPUT_SOURCE, last);
	KcmSetRegister(KCM_INPUT_SOURCE, value1);
    MLOG("SourceW:%d(%d)%02x(%02x)", select, last, value1, value2);
	if (last <= INPUT_SWITCH_COA2){ 								// 如果原来不是抢占式输入
	    KcmSetRegister(KCM_EXTR_MEMORY + MEM_SOURCE_AUTO, value2);	// 自动输入的恢复
		MLOG("SourceJ:%d %02x", select, value2);
	}
}
CONST_WORD TabSrcValid[] = {
	KCM_SRC_VALID_SD, KCM_SRC_VALID_UDISK,
	KCM_SRC_VALID_USBA, KCM_SRC_VALID_BT, KCM_SRC_VALID_E8CH
	// KCM_SRC_VALID_HDMI1, KCM_SRC_VALID_HDMI2, KCM_SRC_VALID_HDMI3
	
}; 
CONST_CHAR TabValidSwitch[] = {
	INPUT_SWITCH_SD, INPUT_SWITCH_UDISK,
	INPUT_SWITCH_USBA, INPUT_SWITCH_BT, INPUT_SWITCH_E8CH
	// INPUT_SWITCH_HDMI1, INPUT_SWITCH_HDMI2, INPUT_SWITCH_HDMI3
};
void MAUD_MakePreemptible(WORD validFlag){			        // 生成抢占式输入选择 
	BYTE qty = 0;
    // if (gSYS_ModelType == KCM_MODEL_35H){                   // 模块版本是KC35H，需要加入外置7.1声道
    //     gPreemptibleIn[qty++] = INPUT_SWITCH_E8CH;
    // }    
    if (validFlag){									// 有抢占式输入 
    	BYTE index;
    	WORD flag;
    	for (index = 0; index < sizeof(TabSrcValid)/2; index++){
        	flag = TabSrcValid[index];

		//BYTE value = KcmGetRegister(KCM_EXTR_MEMORY + MEM_SOURCE_AUTO);	// 自动输入的恢复
    	//select = MKCM_FromRegister(KCM_INPUT_SOURCE, value);				// 从KCM来的寄存器，转换到本机处理的值	
        	if (validFlag & flag){    				// 有非抢占式
			/*
        		if (flag == KCM_SRC_VALID_SD){
					MLOG("SD卡插入YYY (%d)", MENU_SD_INSERT);
					MDIP_MenuNormal(MENU_SD_INSERT); 		// 显示外置音源插入
	        	}else if (flag == KCM_SRC_VALID_UDISK){
					MLOG("U盘插入YYY (%d)", MENU_UD_INSERT);
					MDIP_MenuNormal(MENU_UD_INSERT); 		// 显示外置音源插入
	        	}
				*/
			MLOG("抢占KKKKKKKK (%d)(%d)(%d)",qty, index, TabValidSwitch[index]);
				gPreemptibleIn[qty++] = TabValidSwitch[index];				
        	}
    	}
		if (gWithHdmiQty != qty){						// 带HDMI的抢占式输入  
			gWithHdmiStep = 0;
			gWithHdmiQty = qty;
		}
		for (index = 0; index < gWithHdmiQty; index++){
			if (gPreemptibleIn[index] >= INPUT_SWITCH_HDMI1 && gPreemptibleIn[index] <= INPUT_SWITCH_H_ARC){
				break;
			}
		}
		if (gPreemptibleQty != index){						// 不带HDMI的抢占式输入  
			gPreemptibleQty = qty;
			if(gPreemptibleQty == 1){
				gPreemptibleStep = 0;
			}else{
				++gPreemptibleStep;
			}
			MLOG("抢占CCB (%d)(%d)", gPreemptibleQty, gPreemptibleStep);
		}
    }else {												// 没有了抢占式输入
		gPreemptibleQty = qty;
		gWithHdmiQty = 0; 
			MLOG("抢占CCA");
		gPreemptibleStep = 0;
		gWithHdmiStep = 0;
    }
    g2AUD_SrcValid = validFlag;
}

// SD卡或U盘播放，可以播放返回true
// change 	flase=当前文件 true=不是当前文件
// last 	flase=后一个文件 true=前一个文件
bool MKCM_PlayIndex(bool change, bool last){
	uint8_t qty;
	uint16_t idx;
	if(!change){							
		KcmSetReg16(KCM_PLAY_INDEX, 0xffff);	// 播放上次记忆的曲目
		MLOG("可以播放TTTT ");
		return true;	
	}
	qty = KcmGetReg16(KCM_PLAY_FILE_QTY);
	if (qty > 0){
		if (change){									// 不是当前文件
			idx = KcmGetReg16(KCM_PLAY_INDEX);			// 文件播放编号
			if (!last){
				if (++idx > qty){						// 播放编号大于总数时				
					idx = 0;							// 播放第一首
				}
			}else{
				if (idx > 0){					
					--idx;								// 前一个文件
				}else{
					idx = qty-1;						// 播放最后一首
				}
			}
		}else {
			idx = 0xffff;								// 播放上次记忆的曲目
		}
		KcmSetReg16(KCM_PLAY_INDEX, idx);	
			MLOG("可以播放A qty(%d)", qty);
		return true;		
	}
		MLOG("不可以播放B qty(%d)", qty);
	return false;	
}

void MKCM_ReadSrcInfo(){									// 收到中断读取格式、通道、采样率、码流率等信息
//  MKCM_ReadXByte(KCM_SRC_FORMAT, &gAUD_SrcFormat, 3);   // 连续读3字节到gAUD_SrcFormat gAUD_ChSr gAUD_SrcFreq
	gAUD_SrcFormat = KcmGetRegister(KCM_SRC_FORMAT);	// 数码信号输入格式指示
	gAUD_SrcChannel = KcmGetRegister(KCM_SRC_CHANNEL);	// 数码信号输入通道信息及超低音指示
	gAUD_SrcRate = KcmGetRegister(KCM_SRC_RATE);	// 数码信号输入采样率及实际播放采样率指示
	gAUD_SrcBps = KcmGetRegister(KCM_SRC_BPS);	// 数码信号输入码流率指示
					
	// MLOG("gAUD_SrcFormat %02x %02x %02x", gAUD_SrcFormat, gAUD_ChSr, gAUD_SrcFreq);

//	MLOG("SrcFormat %02x %02x", gAUD_SrcFormat, gAUD_SrcFreq);
	if (mINPUT_SWITCH == INPUT_SWITCH_SD || mINPUT_SWITCH == INPUT_SWITCH_UDISK){
		char fileName[16];
		BYTE length = MKCM_ReadAutoByte(KCM_PLAY_FILE_NAME, fileName, 16);
		// MLOG("FILE_NAME A %d %02x %02x %02x", length, fileName[0], fileName[1], fileName[2]);
		g2TimeLength = KcmGetReg16(KCM_PLAY_FILE_TIME);
	}else {
		if (gDIP_MenuLock == 0){						// 
			if (!FSYS_TestTone){						// 没有打开噪音测试
				MDIP_MenuNormal(MENU_SRC_FORMAT);		// 菜单显示输入码流格式
			}
		}
	}
	if (gAUD_SrcFormat == 0){
		MDIP_CleanSpectrum();
	}
	MDIP_SrcFormatSymbol();
	MDIP_SurroundSymbol();
}

void MKCM_ReadSrcValid(){									// 收到中断读取有效的音源输入改变
	WORD srcValid = KcmGetReg16(KCM_SRC_VALID);         	// 本次的有效音源
	MLOG("收到中断读取BBB (%02x)(%02x)", (uint16_t)srcValid, (uint16_t)g2AUD_SrcValid);
	//uint8_t srcValid = KcmGetReg16(KCM_SRC_VALID);         // 本次的有效音源
	//MLOG("收到中断读取BBB (%02x)(%02x)", srcValid, g2AUD_SrcValid);
    if (g2AUD_SrcValid != srcValid){                       // 本次与上次的有效音源改变
		BYTE index;
    	WORD flag;
    	BOOL found = 0; 
		if(srcValid == 0x60){		// 同时有SD卡和U盘的时候
			MLOG("播放停止AAAA (%02x)(%02x)", srcValid, g2AUD_SrcValid);
			IsDeviceStop = true;
			KcmSetRegister(KCM_PLAY_OPERATE, KCM_OPERATE_STOP); // 多媒体播放停止
		}
        MLOG("KCM_SRC_VALID-AAA:%02x%02x Last:%02x%02x", srcValid>>8, srcValid, g2AUD_SrcValid>>8, g2AUD_SrcValid);
        for (index = 0; index < sizeof(TabSrcValid)/2; index++){
        	flag = TabSrcValid[index];
        	if ((srcValid & flag) && !(g2AUD_SrcValid & flag)){    // 本次有及上次没有非抢占式
		       	found = 1;
	        	switch (flag){
	            case KCM_SRC_VALID_SD:					// 插入SD卡
						MLOG("插入SD卡Y AAA");  
					if (MKCM_PlayIndex(false, false)){
						MLOG("插入SD卡N AAA");  
						MDIP_MenuNormal(MENU_SD_INSERT); 
					}
					break;	
	            case KCM_SRC_VALID_UDISK: 				// 插入U盘
					MLOG("插入U盘K AAA");  
					if (MKCM_PlayIndex(false, false)){
						MLOG("插入U盘L AAA");  
						MDIP_MenuNormal(MENU_UD_INSERT); 				
					}
					break;
	            case INPUT_SWITCH_E8CH: 
	            	found = 0;
					break;	
				// case KCM_SRC_VALID_HDMI1:                    // HDMI1   
				// case KCM_SRC_VALID_HDMI2:                    // HDMI2   
				// case KCM_SRC_VALID_HDMI3:                    // HDMI3   
				// case KCM_SRC_VALID_HDMIS:                    // HDMI-ARC
	            // 	found = 0;
				// 	break;	
      			}
      			if (found){			
					MAUD_InputWrite(TabValidSwitch[index], mINPUT_SWITCH);
					mINPUT_SWITCH = TabValidSwitch[index];
					MLOG("插入的菜单状态 TA (%d)", mINPUT_SWITCH);  
					gDIP_MenuSelect = MENU_RESTORE;			// 菜单即刻进入输入的恢复 
					MDIP_MenuNormal(MENU_INPUT_SOURCE);     // 菜单状态:输入音源选择
					gRemoveTimer = 0;						// 取消拔出恢复到非抢占式插入
			    }
	        	break;										// 只处理一个抢占式插入
        	}
        }
        if (!found){										// 没有非抢占式插入
			uint8_t status = KcmGetRegister(KCM_PLAY_STATE);	
			for (index = 0; index < sizeof(TabSrcValid)/2; index++){
	        	flag = TabSrcValid[index];
	    //MLOG("KCM_SRC_VALID C %d %02x%02x", index, flag>>8, flag);  
				if (!(srcValid & flag) && (g2AUD_SrcValid & flag)){    // 本次没有及上次有非抢占式
		            switch (flag){
		            case KCM_SRC_VALID_SD:					// 拔出SD卡
						if ((status & KCM_STATE_PLAY_PLAY) == KCM_STATE_PLAY_PLAY || (status & KCM_STATE_PAUSE) == KCM_STATE_PAUSE){	// 播放或暂停的状态
							MLOG("拔出SD卡 TA");  
							MDIP_MenuNormal(MENU_SD_REMOVE); 
						}else{
							MLOG("拔出SD卡 TB (%d)", status);  
							MDIP_PlaySymbol(0);					// 灭了对应的显示
							MDIP_MenuNormal(MENU_SD_REMOVE); 
						}
						break;	
		            case KCM_SRC_VALID_UDISK: 				// 拔出U盘	
						if ((status & KCM_STATE_PLAY_PLAY) == KCM_STATE_PLAY_PLAY  || (status & KCM_STATE_PAUSE) == KCM_STATE_PAUSE){	// 播放或暂停的状态
							MLOG("拔出U盘 TY");   
							MDIP_MenuNormal(MENU_UD_REMOVE); 
						}else{
							MLOG("拔出U盘 TT (%d)", status);  
							MDIP_PlaySymbol(0);					// 灭了对应的显示
							MDIP_MenuNormal(MENU_UD_REMOVE); 
						}
						break;	
		            case KCM_SRC_VALID_USBA: 
						MDIP_MenuNormal(MENU_PC_REMOVE); 
						break;	
		            case KCM_SRC_VALID_BT: 
						MDIP_MenuNormal(MENU_BT_REMOVE); 
						break;	
		            // case KCM_SRC_VALID_HDMI1: 
		            // case KCM_SRC_VALID_HDMI2: 
		            // case KCM_SRC_VALID_HDMI3: 
			        //     break;
		            }
					MLOG("拔出的动作A (%d)(%d)qty(%d)(%02x)", mINPUT_SWITCH , TabValidSwitch[index], gPreemptibleQty, srcValid);  
					if (mINPUT_SWITCH == TabValidSwitch[index]){	// (mINPUT_SWITCH)当前的类型和(TabValidSwitch[index])拔出的类型相同
						if(gPreemptibleQty >= 2){
							if(srcValid == 0x20){		// 有SD卡的时候
								MLOG("拔出的动作B 有SD卡)"); 								
								MAUD_InputWrite(INPUT_SWITCH_SD, mINPUT_SWITCH);	// 启动SD卡播放
								mINPUT_SWITCH = INPUT_SWITCH_SD;
							}
							if(srcValid == 0x40){		// 有U盘的时候
								MLOG("拔出的动作C 有U盘)");  
								MAUD_InputWrite(INPUT_SWITCH_UDISK, mINPUT_SWITCH);	// 启动U盘播放
								mINPUT_SWITCH = INPUT_SWITCH_UDISK;
							}
						}else{
							MLOG("拔出的动作D 没有设备)");  
							MDIP_PlaySymbol(0);					// 灭了对应的显示
							gRemoveTimer = 1;					// 大约2秒后 
						}		
			        }					
		            break;									// 只处理一个抢占式拔出
	        	}
	        }
        }
		
		MAUD_MakePreemptible(srcValid);					// 生成抢占式输入选择 
    }else {
        MLOG("KCM_SRC_VALID = Last:%02x%02x", srcValid>>8, srcValid);
    }
}



