

#ifndef __FWB_CONFIG_H
#define __FWB_CONFIG_H

////////////////////////////////////////////////////////////////////////////////////////////////
// 开始 KC3X用户码选择 根据KCM_CUSTOM_CODE寄存器的定义修改CC_xxxx_SELECT为其中一个 /////////////
////////////////////////////////////////////////////////////////////////////////////////////////
	
// 数码输入输出格式数据选择共4种，CC_FORMAT_SELECT选择其中一个
#define CC_FORMAT_I2S				0x00000000				// 选择为I2S格式，默认使用
#define CC_FORMAT_LEFT				0x40000000				// 选择为左对齐格式
#define CC_FORMAT_RIGHT				0x80000000				// 选择为右对齐格式
#define CC_FORMAT_PCM				0xc0000000				// 选择为PCM格式
#define CC_FORMAT_SELECT			CC_FORMAT_I2S			// 选择上面4个的其中1个
		
// 数码时钟输入输出选择共2种，CC_CLOCK_SELECT选择其中一个
#define CC_CLOCK_OUTPUT				0x00000000				// 0 为 BCK 及 WCK 为输出，默认使用
#define CC_CLOCK_INPUT				0x20000000				// 1 为 BCK 及 WCK 为输入	
#define CC_CLOCK_SELECT				CC_CLOCK_OUTPUT			// 选择上面2个的其中1个
		
// 允许话筒声音混合到主声道，CC_MIC_SELECT选择其中一个
#define CC_MIC_DISABLE				0x00000000				// 不允许话筒声音混合到主声道，默认使用
#define CC_MIC_ENABLE				0x10000000				// 允许话筒声音混合到主声道	
#define CC_MIC_SELECT				CC_MIC_ENABLE			// 选择上面2个的其中1个

// 话筒 MIC 与模拟输入交换，CC_SWAP_ADC_SELECT选择其中一个
#define CC_SWAP_ADC_DISABLE			0x00000000				// 不允许话筒 MIC 与模拟输入交换，默认使用
#define CC_SWAP_ADC_ENABLE			0x08000000				// 许话筒 MIC 与模拟输入交换	
#define CC_SWAP_ADC_SELECT			CC_SWAP_ADC_DISABLE		// 选择上面2个的其中1个

// 设置音量芯片类型，CC_VOLUME_SELECT选择其中一个
#define CC_VOLUME_NONE				0x00000000				// 不使用模块内部的音量
#define CC_VOLUME_DSP_1DB			0x00100000				// 使用 DSP 数码音量，每步 1dB
#define CC_VOLUME_DSP_0_5			0x00200000				// 使用 DSP 数码音量，每步 0.5dB
#define CC_VOLUME_PT2258			0x00300000				// 使用 PT2258 + PT2259 或者兼容的音量芯片，默认使用
#define CC_VOLUME_M62446			0x00400000				// 使用 M62446 或者兼容的音量芯片
#define CC_VOLUME_SELECT			CC_VOLUME_PT2258		// 选择上面5个的其中1个

// 允许电平取样及频谱输出，CC_SPECTRUM_SELECT选择其中一个
#define CC_SPECTRUM_DISABLE			0x00000000				// 不允许电平取样及频谱输出
#define CC_SPECTRUM_ENABLE			0x00004000				// 允许电平取样及频谱输出，默认使用	
#define CC_SPECTRUM_SELECT			CC_SPECTRUM_ENABLE		// 选择上面2个的其中1个

// 允许音调及多段EQ均衡音效，CC_TONE_EQ_SELECT选择其中一个
#define CC_TONE_EQ_DISABLE			0x00000000				// 不允许音调及多段EQ均衡音效
#define CC_TONE_EQ_ENABLE			0x00001000				// 允许音调及多段EQ均衡音效，默认使用	
#define CC_TONE_EQ_SELECT			CC_TONE_EQ_ENABLE		// 选择上面2个的其中1个

// 允许各声道音量及声道微调，CC_VOL_TRIM_SELECT选择其中一个
#define CC_VOL_TRIM_DISABLE			0x00000000				// 不允许各声道音量及声道微调
#define CC_VOL_TRIM_ENABLE			0x00000800				// 允许各声道音量及声道微调，默认使用	
#define CC_VOL_TRIM_SELECT			CC_VOL_TRIM_ENABLE		// 选择上面2个的其中1个

// 允许各声道延迟时间及齿音同步，CC_DELAY_LIN_SELECT选择其中一个
#define CC_DELAY_LIN_DISABLE		0x00000000				// 不允许各声道延迟时间及齿音同步
#define CC_DELAY_LIN_ENABLE			0x00000400				// 允许各声道延迟时间及齿音同步，默认使用	
#define CC_DELAY_LIN_SELECT			CC_DELAY_LIN_ENABLE		// 选择上面2个的其中1个

// 允许喇叭设置及低音管理，CC_BASS_MGR_SELECT选择其中一个
#define CC_BASS_MGR_DISABLE			0x00000000				// 不允许喇叭设置及低音管理
#define CC_BASS_MGR_ENABLE			0x00000200				// 允许喇叭设置及低音管理，默认使用	
#define CC_BASS_MGR_SELECT			CC_BASS_MGR_ENABLE		// 选择上面2个的其中1个

// 允许解码输出后的声道下混模式及各种聆听模式，CC_DOWN_MIX_SELECT选择其中一个
#define CC_DOWN_MIX_DISABLE			0x00000000				// 不允许解码输出后的声道下混模式及各种聆听模式
#define CC_DOWN_MIX_ENABLE			0x00000100				// 允许解码输出后的声道下混模式及各种聆听模式，默认使用	
#define CC_DOWN_MIX_SELECT			CC_DOWN_MIX_ENABLE		// 选择上面2个的其中1个

// 允许解码输出后的声道下混模式及各种聆听模式，CC_DOWN_MIX_SELECT选择其中一个
#define CC_USB_SD_DISABLE			0x00000000				// 停止使用SD卡
#define CC_USB_SD_MODE1				0x00000020				// USB及SD卡模式1
#define CC_USB_SD_MODE2				0x00000040				// USB及SD卡模式2
#define CC_USB_SD_SELECT			CC_USB_SD_MODE1			// 选择上面3个的其中1个

// 旧音频板，中置超低音与环绕声 对调  使用PT2258音量IC
#define CC_BOARD_OLD				0x01
#define CC_USER_BOARD               0x00    

////////////////////////////////////////////////////////////////////////////////////////////////
// 结束 KC3X用户码选择 根据KCM_CUSTOM_CODE寄存器的定义修改CC_xxxx_SELECT为其中一个 /////////////
////////////////////////////////////////////////////////////////////////////////////////////////
#define CC_BYTE_3					(CC_FORMAT_SELECT | CC_CLOCK_SELECT | CC_MIC_SELECT | CC_SWAP_ADC_SELECT)
#define CC_BYTE_2					(CC_VOLUME_SELECT)
#define CC_BYTE_1					(CC_SPECTRUM_SELECT | CC_TONE_EQ_SELECT | CC_VOL_TRIM_SELECT | CC_BASS_MGR_SELECT | CC_DOWN_MIX_SELECT)
#define CC_BYTE_0					(CC_USB_SD_SELECT | CC_USER_BOARD)

#define CUSTOM_CODE					(CC_BYTE_3 | CC_BYTE_2 | CC_BYTE_1 | CC_BYTE_0)

	
#define DIP_BRIGHTNESS_MAX 				4					// 屏幕亮度最大值
#define MICROPHONE                      1                   // MIC-EQ调节(1为开，0为关)
#define SPECTRUM_ENABLE                 1                   // 频谱(1为开，0为关)
#define POWER_STANDBY                   0                   // 开机是否开启待机(1为开，0为关)
////////////////////////////////////////////////////////////////
// 显示屏及驱动芯片选择，只能有一个是1，其余要为0
////////////////////////////////////////////////////////////////
#define DISPLAY_LED_TA6932				0
#define DISPLAY_128_64_ST7565			1

////////////////////////////////////////////////////////////////
// 显示屏及驱动芯片选择，只能有一个是1，其余要为0
////////////////////////////////////////////////////////////////
#define REMOTE_NUMBER					1					// 1=我们自己的遥控器

#if (REMOTE_NUMBER == 1)
#define KEY_IR_CUSTOM                   0x00fd0000			// 使用这个是我们自己的遥控器
#endif
#if (REMOTE_NUMBER == 2)
#define KEY_IR_CUSTOM                   0x847b0000			// 使用这个是我们自己的遥控器
#endif

////////////////////////////////////////////////////////////////
// 显示屏及驱动芯片选择，只能有一个是1，其余要为0
////////////////////////////////////////////////////////////////
#define MCU_TYPE_STM32F103 				1
#define MCU_TYPE_W806 					0

#if (DISPLAY_128_64_ST7565 != 0)							
#define DISPLAY_CHINESE					1					// 点阵屏，支持中文显示
#else
#define DISPLAY_CHINESE					0					// 不是点阵屏
#endif	

#define KCM_WR_CMD_MAX					64					// 写KCM指令最大长度 
#define KCM_RD_CMD_MAX					(32+512)				// 读KCM指令最大长度 


#define MAX_STEP_BRIGHT                 3                   // 亮度最大值
#define MAX_STEP_VOLUME                 80                  // 音量最大值


#define PANEL_KEY_LINE					3					// 面板按键扫描行输出数量，现在的代码设计最大为8，超过要修改代码
#define PANEL_KEY_COLUMN				4					// 面板按键扫描行列输入数量，现在的代码设计最大为5，超过要修改代码
#define KEY_JOG_MODE					1					// 旋转按钮模式，0为不支持 1为正向，2为反向	

#define PREVIEW_MUTE					0					// 静音功能 0表示不用预览直接修改 1是需要显示预览再修改
#define PREVIEW_VOLUME					0					// 音量功能 0表示不用预览直接修改 1是需要显示预览再修改
#define PREVIEW_INPUT					0					// 输入功能 0表示不用预览直接修改 1是需要显示预览再修改
#define PREVIEW_NIGHT_MODE				0					// 夜间模式功能 0表示不用预览直接修改 1是需要显示预览再修改



#endif 	// __FWB_CONFIG_H
