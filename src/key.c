
// Copyright (c) 2002-2021, Hard & Soft Technology Co.,LTD.
// SPDX-License-Identifier: Apache-2.0
// https://gitee.com/hsav20/kc3xm51.git
// https://github.com/hsav20/kc3xm51.git
// http://www.hsav.com/download/kc3xm51.zip

#include "main.h"

void MKEY_Initialize(){									// 初始化
	//HAL_STANDBY(0);										// 待机脚变低	
	HAL_STANDBY(1);										// 待机脚变高								  
}
void MKEY_Standby(bool standby){						// 打开待机或关闭待机
	uint8_t value;
	if(standby){										// 打开待机
		MDIP_PlaySymbol(0);								// 灭了对应的显示
		value = KcmGetRegister(KCM_FLAG_CTRL);
		value |= FLAG_CTRL_STANDBY;						// 将B0变为高位1
		MLOG("打开待机AA (%02x)", value); 
		KcmSetRegister(KCM_FLAG_CTRL, value);      		// B0为待机 B0=1 打开待机
		HAL_STANDBY(0);									// 待机脚变低
		MDIP_MenuNormal(MENU_STANDBY);
	}else{												// 关闭待机
		value = KcmGetRegister(KCM_FLAG_CTRL);
		value &= ~FLAG_CTRL_STANDBY;					// 将B0变为低位0 
		MLOG("关闭待机CC %02x ", value);	
		KcmSetRegister(KCM_FLAG_CTRL, value);      		// B0为待机 B0=0关闭待机
		HAL_STANDBY(1);									// 待机脚变高
		MKCM_SetPowerOn();								// KCM开机
	}
}

void MKEY_10msTimer(BYTE baseTimer){   					// B3=1000ms B2=500ms B1=100ms B0=10ms 
	MPKey_Scan();
	if (FSYS_Standby){
		if (FRmDecodDone){
			FRmDecodDone = 0;
			if (GRmCodeData == cRmKey_Standby){
				MLOG("关闭待机BBB ");	
				FSYS_Standby = false;	
				MKEY_Standby(FSYS_Standby);				// 关闭待机
			}
		}
	   	if (FPKeyDecodeDone){
			FPKeyDecodeDone = 0;
			FPKeyStep = 1;
			if (GPKeyData == cPanKey_InputSource){
				MKCM_SetPowerOn();						// KCM开机
			}
		}	
		return;	
	}

   	if (FPKeyDecodeDone){
		FPKeyDecodeDone = 0;
		FPKeyStep = 1;

//MLOG("FPKeyDecodeDone %02x", GRmCodeData);		
		switch (GPKeyData){
		case cPanKey_InputSource:
			MAUD_InputOneKey();         					// 所有输入用一个按键选择 
			break;
		case cPanKey_Stereo:                                // 面板立体声按键
			MKEY_ListenMode(1);                             // 按键聆听模式选择    
			break;
		case cPanKey_SurroundMode:                          // 面板环绕声按键
			MKEY_ListenMode(0);                             // 按键聆听模式选择
			break;
 		case cPanKey_EqSelect:                              // 面板EQ均衡器选择
			MEQMIC_KeyEqSelect();                           // 按键EQ均衡器选择
			break;

		case cPanKey_PlayPause:								// 面板按键 
			MKEY_PlayPause();								// 多媒体播放/暂停
			break;
		case cPanKey_SkipDown:
			MDIP_PlaySkip(true);							// 多媒体播放后一首
			break;
		case cPanKey_SkipUp:
			MDIP_PlaySkip(false);							// 多媒体播放前一首
			break;
  		case cPanKey_AudioMute: 							// MUTE	静音
			if (gDIP_MenuSelect >= MENU_MASTER_VOL && gDIP_MenuSelect <= MENU_MIC_TREBLE){		// 已经在JOG菜单 
				MDIP_MenuNormal(MENU_RESTORE);
			}
			else {
				MKEY_AudioMute();
			}
 			break;

  		case cPanKey_JogMenu:								// JOG
			if (gDIP_MenuSelect < MENU_MASTER_VOL || gDIP_MenuSelect > MENU_MIC_VOL2){		// 还没有进入JOG菜单 
				MDIP_MenuNormal(MENU_MASTER_VOL);
			}
			else {
				if (++gDIP_MenuSelect > MENU_MIC_VOL2){
					gDIP_MenuSelect = MENU_MASTER_VOL;
				}
				MDIP_MenuNormal(gDIP_MenuSelect);
			}
			break;
		}
	}
    
    if ((gRmKeyContinCanclTm != 0) && (++gRmKeyContinCanclTm > 60)){	
        gRmKeyContinCanclTm = 0;
		gRmKeyContinSpeed = 0;
    }
	if (FRmDecodDone){
		BYTE gLocal_1;
		BYTE value;
		FRmDecodDone = 0;
//MLOG("FRmDecodDone %02x ", GRmCodeData);	
		switch (GRmCodeData){
		case cRmKey_Standby:								// 待机
			FSYS_Standby = true;	
			MKEY_Standby(FSYS_Standby);						// 打开待机
			break;
		case cRmKey_Brightness:								// 亮度调节
			gLocal_1 = (gDIP_MenuSelect == MENU_BRIGHTNESS) ? MENU_SET_ADJ_UP : MENU_SET_NORMAL;
			MDIP_MenuSelect(MENU_BRIGHTNESS, gLocal_1);
			break;
		case cRmKey_FactorySet:								// 出厂设置
			MDIP_WrString(" RESET");
			MDIP_ScreenUpdata();
			MKCM_FactorySet();
			break;
		case cRmKey_MicCtrl:
			MEQMIC_KeyCtrl();								// EQ或MIC按键CTRL入口
			break;
		case cRmKey_TrimUp:
				MLOG("CCCC");	
            if (gDIP_MenuSelect >= MENU_CH_TRIM_FL && gDIP_MenuSelect <= MENU_CH_TRIM_SL){	// 已经进入声道微调菜单 
                gRmKeyContinCanclTm = 1;
				MDIP_MenuSelect(gDIP_MenuSelect, MENU_SET_ADJ_UP);
            }else if (gDIP_MenuSelect >= MENU_TESTTONE_FL && gDIP_MenuSelect <= MENU_TESTTONE_SL){	// 已经进入测试噪音菜单 
				gRmKeyContinCanclTm = 1;
				MDIP_MenuSelect(gDIP_MenuSelect, MENU_SET_ADJ_UP);
			}
			break;
		case cRmKey_TrimDown:
            if (gDIP_MenuSelect >= MENU_CH_TRIM_FL && gDIP_MenuSelect <= MENU_CH_TRIM_SL){	// 已经进入声道微调菜单 
                gRmKeyContinCanclTm = 1;
				MDIP_MenuSelect(gDIP_MenuSelect, MENU_SET_ADJ_DOWN);
            }else if (gDIP_MenuSelect >= MENU_TESTTONE_FL && gDIP_MenuSelect <= MENU_TESTTONE_SL){	// 已经进入测试噪音菜单 
				gRmKeyContinCanclTm = 1;
				MDIP_MenuSelect(gDIP_MenuSelect, MENU_SET_ADJ_DOWN);
			}
			break;
		case cRmKey_MicUp:
			MEQMIC_KeyUp();									// EQ或MIC按键调节+入口
			break;
		case cRmKey_MicDown:
			MEQMIC_KeyDown();								// EQ或MIC按键调节-入口
			break;
		case cRmKey_VolumeUp:								// 音量+
			gRmKeyContinCanclTm = 1;
			gLocal_1 = (gDIP_MenuSelect == MENU_MASTER_VOL) ? MENU_SET_ADJ_UP : MENU_SET_NORMAL;
			MDIP_MenuSelect(MENU_MASTER_VOL, gLocal_1);
			break;
		case cRmKey_VolumeDown:								// 音量-
			if (!FSYS_MuteEnable){
				gRmKeyContinCanclTm = 1;
				gLocal_1 = (gDIP_MenuSelect == MENU_MASTER_VOL) ? MENU_SET_ADJ_DOWN : MENU_SET_NORMAL;
				MDIP_MenuSelect(MENU_MASTER_VOL, gLocal_1);
			}
			break;
		case cRmKey_TestTone:								// 噪音测试
			MKEY_TestTone();
			break;
		case cRmKey_AudioMute:								// 静音
    		MKEY_AudioMute();
			break;
		case cRmKey_MeunCtrl:
			if (gDIP_MenuSelect >= MENU_LIP_SYNC && gDIP_MenuSelect <= MENU_SPEAK_FILTER){	// 已经进入设置菜单 
				MDIP_MenuNormal(MENU_RESTORE);
			}
			else {
				MDIP_MenuNormal(MENU_LIP_SYNC);
			}
			break;
		case cRmKey_MeunUp:
			if (gDIP_MenuSelect >= MENU_LIP_SYNC && gDIP_MenuSelect <= MENU_SPEAK_FILTER){	// 已经进入设置菜单 
				++gDIP_MenuSelect;
				if (gDIP_MenuSelect == MENU_DELAY_CENTER){	// 菜单中置声道延迟时间调整
					if (gDIP_SpeakSetup[1] == 0){			// 没有中置喇叭
						++gDIP_MenuSelect;					// 跳过
					}
				}
				if (gDIP_MenuSelect == MENU_DELAY_SURR){	// 菜单环绕声道延迟时间调整
					if (gDIP_SpeakSetup[3] == 0){			// 没有环绕声喇叭
						++gDIP_MenuSelect;					// 跳过
					}
				}
				if (gDIP_MenuSelect == MENU_DELAY_BACK){	// 菜单后置声道延迟时间调整
					if (gDIP_SpeakSetup[4] == 0){			// 没有后置喇叭
						++gDIP_MenuSelect;					// 跳过
					}
				}
				if (gDIP_MenuSelect > MENU_SPEAK_FILTER){
					gDIP_MenuSelect = MENU_LIP_SYNC;
				}
				MDIP_MenuNormal(gDIP_MenuSelect);
				gDIP_MenuTimer = 50;
			}
//MDEBUG(0xa9);MDEBUG(gDIP_MenuSelect);
			break;
		case cRmKey_MeunDown:
			if (gDIP_MenuSelect >= MENU_LIP_SYNC && gDIP_MenuSelect <= MENU_SPEAK_FILTER){	// 已经进入设置菜单 
				--gDIP_MenuSelect;
				if (gDIP_MenuSelect == MENU_DELAY_BACK){	// 菜单后置声道延迟时间调整
					if (gDIP_SpeakSetup[4] == 0){			// 没有后置喇叭
						--gDIP_MenuSelect;					// 跳过
					}
				}
				if (gDIP_MenuSelect == MENU_DELAY_SURR){	// 菜单环绕声道延迟时间调整
					if (gDIP_SpeakSetup[3] == 0){			// 没有环绕声喇叭
						--gDIP_MenuSelect;					// 跳过
					}
				}

				if (gDIP_MenuSelect == MENU_DELAY_CENTER){	// 菜单中置声道延迟时间调整
					if (gDIP_SpeakSetup[1] == 0){			// 没有中置喇叭
						--gDIP_MenuSelect;					// 跳过
					}
				}
				if (gDIP_MenuSelect < MENU_LIP_SYNC){
					gDIP_MenuSelect = MENU_SPEAK_FILTER;
				}
				MDIP_MenuNormal(gDIP_MenuSelect);
				gDIP_MenuTimer = 50;
			}
//MDEBUG(0xa8);MDEBUG(gDIP_MenuSelect);
			break;
		case cRmKey_MeunLeft:
			if (gDIP_MenuSelect >= MENU_LIP_SYNC && gDIP_MenuSelect <= MENU_SPEAK_FILTER){	// 已经进入设置菜单 
				gRmKeyContinCanclTm = 1;
				MDIP_MenuSelect(gDIP_MenuSelect, MENU_SET_ADJ_DOWN);
			}
			break;
		case cRmKey_MeunRight:
			if (gDIP_MenuSelect >= MENU_LIP_SYNC && gDIP_MenuSelect <= MENU_SPEAK_FILTER){	// 已经进入设置菜单 
				gRmKeyContinCanclTm = 1;
				MDIP_MenuSelect(gDIP_MenuSelect, MENU_SET_ADJ_UP);
			}
			break;
		case cRmKey_TrimCtrl:                              		 // 声道微调
			if (gDIP_MenuSelect < MENU_CH_TRIM_FL || gDIP_MenuSelect > MENU_CH_TRIM_SL){	// 还没有进入声道微调菜单 
            	MAUD_AutoCanclTestTone();
				MDIP_MenuNormal(MENU_CH_TRIM_FL);   			// 进入声道微调菜单 
			}else {                                         	// 已经进入声道微调菜单 
                BYTE next = MDIP_GetNextChannel(gDIP_MenuSelect - MENU_CH_TRIM_FL);    
//MDEBUG(0xee);MDEBUG(gDIP_MenuSelect - MENU_CH_TRIM_FL);MDEBUG(next);//MDEBUG(gDIP_SpeakSetup[1]);MDEBUG(gDIP_SpeakSetup[2]);MDEBUG(gDIP_SpeakSetup[3]);MDEBUG(gDIP_SpeakSetup[4]);MDEBUG(gDIP_MenuSelect);MDEBUG(MENU_CH_TRIM_FL);
                gDIP_MenuSelect = next + MENU_CH_TRIM_FL;
				MDIP_MenuNormal(gDIP_MenuSelect);
			}
			break;
		case cRmKey_Record:                                        // 录音及暂停功能，现在暂时用来测试I2C总线读取
			// FDIP_ScreenFill = ~FDIP_ScreenFill;
			// if (FDIP_ScreenFill){
			// 	MDIP_ScreenFill(0xff);	
			// 	MDIP_ScreenUpdata();
			// }else {
			// 	MDIP_SurroundSymbol();
			// 	MDIP_SrcFormatSymbol();
			// 	MDIP_MenuNormal(MENU_RESTORE);
			// }
			break;
		case cRmKey_Random:											// 随机
			// KcmSetRegister(KCM_PLAY_OPERATE, KCM_PLAY_RANDOM0);  // 多媒体随机播放，时间0
			break;
		case cRmKey_Repeat:											// 重复
			// KcmSetRegister(KCM_PLAY_OPERATE, KCM_PLAY_REPEAT0);  // 多媒体重复播放，类型0
			break;

		case cRmKey_Stop:										// 按键停止
			KcmSetRegister(KCM_PLAY_OPERATE, KCM_OPERATE_STOP); // 多媒体播放停止
			break;
		case cRmKey_PlayPause:									// 按键播放/暂停
			MKEY_PlayPause();
			break;
		case cRmKey_FastBack:
			//KcmSetRegister(KCM_PLAY_OPERATE, KCM_OPERATE_FAST_BACK);  // 多媒体播放播放快退
			break;
		case cRmKey_FastForward:
			//KcmSetRegister(KCM_PLAY_OPERATE, KCM_OPERATE_FAST_FORW);  // 多媒体播放快进
			break;
		case cRmKey_SkipDown:
			MDIP_PlaySkip(true);							// 多媒体播放后一首
			break;
		case cRmKey_SkipUp:
			MDIP_PlaySkip(false);							// 多媒体播放前一首
			break;
		case cRmKey_EqSelect:
            MEQMIC_KeyEqSelect();                           // 按键EQ均衡器选择
			break;
		case cRmKey_NightMode:
			MDIP_MenuNormal(MENU_NIGHT_MODE);
			break;
		case cRmKey_NoiseSignal:
			MDIP_MenuNormal(MENU_TEST_TONE);
			break;
		case cRmKey_MediaType:
			break;
		case cRmKey_Stereo:                                 // 面板立体声按键
            MKEY_ListenMode(1);                             // 按键聆听模式选择
			break;
		case cRmKey_Surround:                               // 面板环绕声按键
            MKEY_ListenMode(0);                             // 按键聆听模式选择
			break;
		case cRmKey_VideoSrc:								// 视频
			MKEY_VideoSelect();
			break;
		case cRmKey_InputNet:								// 云音乐(切换UDisk或SD卡输入)
			MAUD_Preemptible();								// 抢占式输入选择 			
			break;
		case cRmKey_InputHdmi1:
			MAUD_InputSelect(INPUT_SWITCH_HDMI1);
			break;
		case cRmKey_InputHdmi2:
			MAUD_InputSelect(INPUT_SWITCH_HDMI2);
			break;
		case cRmKey_InputHdmi3:
			MAUD_InputSelect(INPUT_SWITCH_HDMI3);
			break;
		case cRmKey_InputHdmiArc:							// HDMI_ARC或外置7.1输入
			MAUD_InputSelect(INPUT_SWITCH_E8CH);			// 外置7.1声道
			// MAUD_InputSelect(INPUT_SWITCH_H_ARC);
			break;
		case cRmKey_InputOptica:
			MAUD_InputSelect(INPUT_SWITCH_OPTIC);
			break;
		case cRmKey_InputCoaxal1:
			MAUD_InputSelect(INPUT_SWITCH_COA1);
			break;
		case cRmKey_InputCoaxal2:
			MAUD_InputSelect(INPUT_SWITCH_COA2);
			break;
		case cRmKey_InputAux:
			MAUD_InputSelect(INPUT_SWITCH_AUX);
			break;
		}
	}
	if (FKeyJogSwOK){
		FKeyJogSwOK = 0;
		if (gDIP_MenuSelect >= MENU_MASTER_VOL && gDIP_MenuSelect <= MENU_MIC_VOL2){		// 已经进入JOG菜单 
			MDIP_MenuSelect(gDIP_MenuSelect, FKeyJogUp ? MENU_SET_ADJ_UP : MENU_SET_ADJ_DOWN);
		}
		else {
			MDIP_MenuNormal(MENU_MASTER_VOL);
		}
	}
    return;
}	  
void MKEY_AudioMute(){										// 静音
	BYTE gLocal_1;
	FSYS_MuteEnable = ~FSYS_MuteEnable;
	gLocal_1 = FSYS_MuteEnable ? 4 : 3;
	MDIP_MenuSelect(MENU_AUDIO_MUTE, gLocal_1);
	if (!FSYS_MuteEnable){									// 不是静音
		MDIP_MenuNormal(MENU_MASTER_VOL);
	}
    MAUD_AutoCanclTestTone();
    return;
}
void MKEY_TestTone(){                                       // MENU_SET mode
	FSYS_TestTone = ~FSYS_TestTone;
	if (FSYS_TestTone){										// 打开噪音测试
		MDIP_MenuNormal(MENU_TESTTONE_FL);
        MAUD_TestToneChannel(0);

	}
	else {													// 关闭噪音测试
		MDIP_MenuNormal(MENU_RESTORE);
		MAUD_TestToneChannel(0xff);
	}
    return;
}
void MKEY_PlayPause(){										// 按键播放/暂停
	if (mINPUT_SWITCH == INPUT_SWITCH_SD || mINPUT_SWITCH == INPUT_SWITCH_UDISK){
		uint8_t status = KcmGetRegister(KCM_PLAY_STATE);
		if (status & KCM_STATE_PLAY_PLAY){						// 如果已经在播放之中
			KcmSetRegister(KCM_PLAY_OPERATE, KCM_STATE_PAUSE);  // 暂停	
			status |= KCM_STATE_PAUSE;
		}else {
			KcmSetRegister(KCM_PLAY_OPERATE, KCM_STATE_PLAY_PLAY);  // 播放
			status &= ~KCM_STATE_PLAY_PLAY;
		}
		MDIP_PlaySymbol(status);								// 加快图标显示，让操作感觉好点
	}
}

CONST_CHAR Tab_VideoSelect[] = {
	0x01,0x02,0x04,
};
void MKEY_VideoSelect(){
	if (++gVideoSelect >= 3){
		gVideoSelect = 0;
	}
	KcmSetRegister(KCM_INPUT_VIDEO, 1 << gVideoSelect);
	MDIP_MenuNormal(MENU_VIDEO_SRC);
	// if (mINPUT_SWITCH < INPUT_SWITCH_HDMI1){
	// 	BYTE gLocal_1 = gAUD_AutoInputSrc >> 5;					// B7:B5为选择的视频
	// 	BYTE gLocal_2 = gAUD_AutoInputSrc & 0x07;				// B4:B0为插入的视频位
	// 	BYTE gLocal_3 = 0;
	// 	do {
	// 		if (++gLocal_1 > 2){
	// 			gLocal_1 = 0;
	// 		}
	// 		if (gLocal_2 & Tab_VideoSelect[gLocal_1]){			// 选择的视频有插入
	// 			KcmSetRegister(KCM_INPUT_VIDEO, (gLocal_1 << 5) | gLocal_2);
	// 		    return;
	// 		}
	// 	} while (++gLocal_3 < 3);
	//     MDIP_MenuNormal(MENU_VIDEO_SRC);
	// }
	// else {
	// 	MDIP_MenuNormal(MENU_RESTORE);
	// }
}
// CONST_CHAR Tab_ListenModeRegister[] = {
//     KCM_LISTEN_STEREO, 										// 双声道立体声，关闭超低音
// 	KCM_LISTEN_STEREO | KCM_LISTEN_ENA_SW, 					// 双声道立体声，打开超低音
// 	KCM_LISTEN_MULTI, 										// 选择多声道源码模式，没有任何多声道算法，B2:0为0
// 	KCM_LISTEN_SURROUND | 0 | KCM_LISTEN_ENA_SW, 			// 选择多声道模式，算法0，打开超低音
// 	KCM_LISTEN_SURROUND | 1, 								// 选择多声道模式，算法1
// 	KCM_LISTEN_SURROUND | 1 | KCM_LISTEN_ENA_SW, 			// 选择多声道模式，算法1，打开超低音
// 	KCM_LISTEN_EFFECT | 0 | KCM_LISTEN_ENA_SW, 				// 选择多声道音效，算法0，打开超低音
// };
// LISTEN_MODE_STATE GetListenModeIndex(BYTE value){
//     switch (value & KCM_LISTEN_MASK){                       // 选择聆听模式位掩码
//     case KCM_LISTEN_STEREO: 								// 选择为双声道立体声，B0为0关闭超低音；为1打开超低音
// 		if (value == Tab_ListenModeRegister[LISTEN_MODE_HIFI]){
// 			return LISTEN_MODE_HIFI;
// 		}
// 		return LISTEN_MODE_2_1CH;   
//     case KCM_LISTEN_MULTI: 
// 		return LISTEN_MODE_SURROUND1;                		// 选择为多声道源码模式，没有任何多声道算法
//     case KCM_LISTEN_SURROUND: 
// 		if (value == Tab_ListenModeRegister[LISTEN_MODE_SURROUND2]){
// 			return LISTEN_MODE_SURROUND2;
// 		}
// 		if (value == Tab_ListenModeRegister[LISTEN_MODE_SURROUND3]){
// 			return LISTEN_MODE_SURROUND3;
// 		}
// 		return LISTEN_MODE_SURROUND4;  						// 选择多声道模式，B1:0为各种不同算法的多声道模式
//     }
//     return LISTEN_MODE_SURROUND5;                           // 选择多声道音效，B1:0为各种不同算法的多声道音效
// }

void MKEY_ListenMode(BYTE stereo){                          // 按键聆听模式选择
    BYTE value = KcmGetRegister(KCM_LISTEN_MODE);        	// 聆听模式选择
    if (gDIP_MenuSelect == MENU_LISTEN_MODE){               // 只有进入对应的菜单才改变模式
        LISTEN_MODE_STATE state = (LISTEN_MODE_STATE)MKCM_FromRegister(KCM_LISTEN_MODE, value);
        if (stereo){                                        // 选择立体声
            state = (state == LISTEN_MODE_HIFI) ? LISTEN_MODE_2_1CH : LISTEN_MODE_HIFI;
        }else {
            if (state < LISTEN_MODE_SURROUND1){             // 只有在立体声才直接换到MODE1
                state = LISTEN_MODE_SURROUND1;
            }else if (++state > LISTEN_MODE_SURROUND5){
                state = LISTEN_MODE_SURROUND1;
            }
        }
        value = MKCM_ToRegister(KCM_LISTEN_MODE, state);
        KcmSetRegister(KCM_LISTEN_MODE, value);
		//MLOG("ListenAV %d %02x", state, value);		
    }
    MDIP_ListenMode(value);
}

