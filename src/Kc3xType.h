
#ifndef __KC3X_TYPE_H
#define __KC3X_TYPE_H


typedef enum {
	KCM_READ_IRQ = 0x01,				// 读中断请求寄存器，16位寄存器
	KCM_CLEAR_IRQ = 0x03,				// 清除中断请求寄存器，16位寄存器
	KCM_POWER_ON = 0x05,				// 模块上电开机、重新同步、待机及重启寄存器
	KCM_FLAG_CTRL = 0x06,				// 标志 B0=STANDBY B1=MUTE B2=杜比数码动态压缩
//	KCM_VOLUME_MUTE = 0x08,				// 音频静音及音量加减控制
	KCM_TEST_TONE = 0x09,				// 噪音测试
	KCM_SRC_DETECT = 0x0a,				// 检测所有有效的音源一次
	KCM_ERROR_FLAG = 0x10,				// 获取模块错误及解码标志，32位寄存器
	KCM_SRC_FORMAT = 0x18,				// 数码信号输入格式指示
	KCM_SRC_CHANNEL = 0x19,				// 数码信号输入通道信息及超低音指示
	KCM_SRC_RATE = 0x1a,				// 数码信号输入采样率及实际播放采样率指示
	KCM_SRC_BPS = 0x1b,					// 数码信号输入码流率指示
	KCM_SRC_VALID = 0x1c,				// 有效的音源输入改变，16位寄存器
	KCM_WORK_STATUS = 0x1f,				// 模块工作/运行状态指示

	// 以下为带上电记忆的寄存器
	KCM_INPUT_SOURCE = 0x20,			// 输入音源选择
	KCM_INPUT_VIDEO = 0x21,				// 输入视频源选择
//	KCM_DYN_COMPRES = 0x23,				// 杜比数码动态压缩
	KCM_SPK_CONFIG = 0x24,				// 喇叭设置
	KCM_LPF_FREQ = 0x25,				// 超低音通道LPF低通滤波器频率
	KCM_HPF_FREQ = 0x26,				// 主声道小喇叭HPF高通滤波器频率
	KCM_LIP_SYNC_SET = 0x28,			// 齿音同步延迟时间，修正对画面与声音不同步
	KCM_LIP_SYNC_MAX = 0x29,			// 齿音同步最大的延迟时间
	KCM_LISTEN_MODE = 0x2b,				// 聆听模式选择
	KCM_EQ_SELECT = 0x2c,				// 多路均衡音效处理选择
	KCM_MEM_BRIGHT = 0x2d,				// 记忆用 用户主机面板亮度控制，一般用于从远程返回主机
	KCM_VOLUME_MAX = 0x2e,				// 设置音量最大值
	KCM_VOLUME_CTRL = 0x2f,				// 音量值设置
	KCM_FL_TRIM = 0x30,					// 前置左声道微调 B4=1为减dB B3-B0为dB数值
	KCM_FR_TRIM = 0x31,					// 前置右声道微调 B4=1为减dB B3-B0为dB数值
	KCM_CE_TRIM = 0x32,					// 中置声道微调	  B4=1为减dB B3-B0为dB数值
	KCM_SW_TRIM = 0x33,					// 超低音声道微调 B4=1为减dB B3-B0为dB数值
	KCM_SL_TRIM = 0x34,					// 环绕左声道微调 B4=1为减dB B3-B0为dB数值
	KCM_SR_TRIM = 0x35,					// 环绕右声道微调 B4=1为减dB B3-B0为dB数值
	KCM_BL_TRIM = 0x36,					// 后置左声道微调 B4=1为减dB B3-B0为dB数值
	KCM_BR_TRIM = 0x37,					// 后置右声道微调 B4=1为减dB B3-B0为dB数值
	KCM_TL_TRIM = 0x38,					// 顶部左声道微调 B4=1为减dB B3-B0为dB数值
	KCM_TR_TRIM = 0x39,					// 顶部右声道微调 B4=1为减dB B3-B0为dB数值
	KCM_MIC_MIXER = 0x3a,				// 话筒声音与主声道合成比例
	KCM_MIC_VOLUME = 0x3b,				// 话筒1及话筒2音量比例
	KCM_MIC_ECHO = 0x3c,				// 话筒直达声及回声比例
	KCM_MIC_DELAY = 0x3d,				// 话筒延迟时间及重复比例
	KCM_MIC_REVERB = 0x3e,				// 话筒混响1及混响2比例
	KCM_MIC_WHISTLE = 0x3f,				// 话筒啸叫声音反馈模式
	KCM_EXTR_MEMORY = 0x40,				// 扩展给用户主机的掉电记忆空间，0x40-0x7f共64字节（带上电记忆）

	// 以下为多字节读写寄存器，这些寄存器不会自动增加寄存器的索引值
	KCM_CUSTOM_CODE = 0x80,				// 设置用户自定义功能寄存器
	KCM_RD_INFO = 0x81,					// 读取模块信息寄存器
	KCM_FW_UPGRADE = 0x82,				// 升级模块固件寄存器
	KCM_RD_RAM = 0x83,					// 读取指定地址的RAM内容
	KCM_MAX_DELAY = 0x86,				// 读取所有声道最大可用的延迟时间
	KCM_DELAY_TIME = 0x87,				// 设置所有声道的延迟

	KCM_EQ_SETUP = 0x8b,				// 多段EQ均衡音效处理设置
	KCM_EQ_VALUE = 0x8c,				// 多段EQ均衡音效处理数值
	KCM_MIC_ADJ_MAX = 0x8d,				// 话筒各种参数最大值设置
	KCM_WR_SPECTRUM = 0x90,				// 设置频谱模式
	KCM_RD_SPECTRUM = 0x91,				// 频谱数值读取
	KCM_RD_SERIAL_NO = 0x98,			// 读取序号

	KCM_PLAY_FILE_QTY = 0xa1,			// 读取多媒体文件总数量，共2字节
	KCM_PLAY_FILE_TIME = 0xa2,			// 读取正在播放文件的总时间，共2字节单位秒
	KCM_PLAY_TIME = 0xa3,				// 读取正在播放的实际时间，共2字节单位秒
	KCM_PLAY_INDEX = 0xa4,				// 读取/写入文件播放编号，共2字节
	KCM_PLAY_STATE = 0xa5,				// 读取/写入文件播放状态，共1字节
	KCM_PLAY_OPERATE = 0xa6,			// 读取/写入文件播放控制，共1字节
	KCM_PLAY_FILE_NAME = 0xa7,			// 读取当前多媒体文件名/歌曲名，最多32字节

	KCM_WR_COMMAND = 0xb0,				// 写入指令，多字节
	KCM_RD_COMMAND = 0xb1,				// 读取指令，多字节

	KCM_REGISTER_LAST = 0xbf
} KC3X_REGISTER;						// 寄存器索引值

typedef enum {
	KCM_IRQ_SYSTEM_INIT = 0x0001,       // 模式初始化完成中断，可以读取一些寄存器恢复本地的记忆
	KCM_IRQ_FORMAT_INFO = 0x0002,       // 输入信号格式改变中断，需要读取"KCM_SRC_FORMAT"寄存器
	KCM_IRQ_SRC_VALID = 0x0008,        	// 有效的音源输入改变中断，需要读取"KCM_SRC_VALID"寄存器

	KCM_IRQ_FIRMWARE = 0x10,        	// 固件更新，需要读取"KCM_RD_INFO"寄存器
	KCM_IRQ_PLAY_STATE = 0x0020,       	// 多媒体文件播放状态改变，需要读取"KCM_PLAY_STATE"寄存器
	KCM_IRQ_PLAY_TIME = 0x0040,        	// 多媒体播放时间改变，需要读取"KCM_PLAY_TIME"寄存器
	KCM_IRQ_COMMAND = 0x0080  			// 主机有读指令变化，需要读取"KCM_RD_COMMAND"寄存器
} KC3X_IRQ_TYPE;

typedef enum {
	KCM_SET_STANDBY = 0x00,       		// 设置模块进入待机状态，成功待机后状态改变为KCM_IS_STANDBY
	KCM_SET_POWER_ON = 0x01,       		// 设置模块进入正常工作状态，成功待机后状态改变为KCM_IS_POWER_ON
	KCM_SET_REBOOT = 0x55,            	// 设置模块重启，成功待机后状态改变为KCM_SET_POWER_ON
	KCM_IS_STANDBY = 0x80,        		// 读取到模块进入待机状态
	KCM_IS_POWER_ON = 0x81        		// 读取到模块进入正常工作状态
} KC3X_POWER_ON;

typedef enum {
	KCM_SRC_NOS = 0x00,					// 输入没有信号
	KCM_SRC_PCM = 0x01,					// PCM信号输入
	KCM_SRC_AC3 = 0x02,					// 标准的AC3信号输入
	KCM_SRC_DTS = 0x03,					// 标准的DTS/DTS-CD信号输入
	KCM_SRC_AAC = 0x04,					// AAC信号输入
	KCM_SRC_MPEG2 = 0x05,				// MPEG2多声道信号输入
	KCM_SRC_DSD = 0x06,					// DSD信号输入
	KCM_SRC_MP3 = 0x07,					// MP3信号输入
	KCM_SRC_SBC = 0x08,					// SBC蓝牙信号输入
	KCM_SRC_LPCM = 0x11,				// LPCM信号输入
	KCM_SRC_HDCD = 0x21,				// HD-CD信号输入
	KCM_SRC_EAC3 = 0x12,				// Enhanced AC-3信号输入
	KCM_SRC_TRUEHD = 0x22,				// 杜比TRUE HD信号输入
	KCM_SRC_MLP = 0x32,					// DVD AUDIO MLP信号输入
	KCM_SRC_DTS_ES = 0x13,				// DTS Extended Surround信号输入
	KCM_SRC_DTS_MA = 0x23,				// DTS HD Master Audio信号输入
	KCM_SRC_DTS_HRA = 0x33,				// DTS HD High Resolution Audio信号输入
	KCM_SRC_ATMOS = 0x80				// 信号包含杜比全景声 
} KC3X_SRC_TYPE;

typedef enum {
	KCM_INPUT_ANALOG = 0x00,			// 音源选择模拟输入
	KCM_INPUT_DIGITAL = 0x10,			// 音源选择数码输入
	KCM_INPUT_RX1 = 0x10,				// 音源选择RX1输入
	KCM_INPUT_RX2 = 0x11,				// 音源选择RX2输入
	KCM_INPUT_RX3 = 0x12,				// 音源选择RX3输入
	KCM_INPUT_HDMI = 0x20,				// 音源选择HDMI输入
	KCM_INPUT_HDMI1 = 0x20,				// 音源选择HDMI1输入
	KCM_INPUT_HDMI2 = 0x21,				// 音源选择HDMI2输入
	KCM_INPUT_HDMI3 = 0x22,				// 音源选择HDMI3输入
	KCM_INPUT_ARC = 0x24,				// 音源选择HDMI ARC输入
	KCM_INPUT_MEDIA = 0x30,				// 音源选择U盘、TF卡、蓝牙、WIFI、USB声卡、网络等输入
	KCM_INPUT_SD = 0x30,				// 音源选择SD/TF卡输入
	KCM_INPUT_UDISK = 0x31,				// 音源选择U盘输入
	KCM_INPUT_PC = 0x32,				// 音源选择USB声卡输入
	KCM_INPUT_E8CH = 0x33,				// 音源选择外置7.1声道输入
	KCM_INPUT_BT = 0x34,				// 音源选择蓝牙输入
	KCM_INPUT_SIGNAL = 0x40				// 音源选择内部产生信号
} KC3X_INPUT_TYPE;

typedef enum {
	KCM_SRC_VALID_ANALOG = 0x0001,		// 有信号的音源输入：模拟输入
	KCM_SRC_VALID_RX1 = 0x0002,			// 有信号的音源输入：数码1
	KCM_SRC_VALID_RX2 = 0x0004,			// 有信号的音源输入：数码2
	KCM_SRC_VALID_RX3 = 0x0008,			// 有信号的音源输入：数码3

	KCM_SRC_VALID_SD = 0x0020,			// 有文件的音源输入：SD插入
	KCM_SRC_VALID_UDISK = 0x0040,		// 有文件的音源输入：U盘插入
	KCM_SRC_VALID_MIC = 0x0080,			// 有信号的音源输入：话筒插入
	
	KCM_SRC_VALID_HDMI1 = 0x0100,		// 有信号的音源输入：HDMI1
	KCM_SRC_VALID_HDMI2 = 0x0200,		// 有信号的音源输入：HDMI2
	KCM_SRC_VALID_HDMI3 = 0x0400,		// 有信号的音源输入：HDMI3
	KCM_SRC_VALID_HDMIS = 0x0f00,		// HDMI位掩码

	KCM_SRC_VALID_USBA = 0x1000,		// 有信号的音源输入：USB声卡
	KCM_SRC_VALID_E8CH = 0x2000,		// 有信号的音源输入：外置7.1声道
	KCM_SRC_VALID_BT = 0x4000,			// 有信号的音源输入：蓝牙音频
	KCM_SRC_VALID_WIFI = 0x8000			// 有信号的音源输入：WIFI音频
} KC3X_SRC_VALID;

typedef enum {
	KCM_LISTEN_ENA_SW = 0x80,			// 允许超低音标志
	KCM_LISTEN_STEREO = 0x00,			// 选择为双声道立体声，B2:0为0
	KCM_LISTEN_MULTI = 0x10,			// 选择多声道源码模式，没有任何多声道算法，B2:0为0
	KCM_LISTEN_SURROUND = 0x20,			// 选择多声道模式，当前支持0至3共4种不同算法的多声道模式
	KCM_LISTEN_EFFECT = 0x30,			// 选择多声道混响音效，当前支持0至3共4种不同算法的多声道音效
	KCM_LISTEN_MASK = 0x70,				// 选择聆听模式位掩码
	KCM_LISTEN_NUMBER = 0x0f			// 选择聆听算法位掩码 
} KC3X_LISTEN_FLAG;

typedef enum {
	KCM_ERROR_TEMP0 = 0x00000001,							// B1:0模块温度范围
	KCM_ERROR_TEMP1 = 0x00000002,							// B1:0模块温度范围
	KCM_ERROR_SPDIF = 0x00000004,							// B2模块SPDIF接口芯片、时钟不正常;
	KCM_ERROR_CODEC = 0x00000008,							// B3模块ADC/DAC硬件初始化不正常;
	KCM_ERROR_VOLUME = 0x00000010,							// B4模块控制音量芯片硬件初始化不正常;
	KCM_ERROR_MIC = 0x00000020,								// B5话筒音量芯片硬件初始化不正常;
	KCM_ERROR_EQ = 0x00000040,								// B6多段EQ均衡音效初始化不正常，可能超出最大的预设计段数;
	KCM_ERROR_WFBT = 0x00000080,							// B7音频模块WIFI蓝牙芯片通讯不正常
	KCM_ERROR_UNICP = 0x00002000,							// B13字符集文件UNICPxxxx.fwb不存在或缺失出错
	KCM_ERROR_UNICF = 0x00004000,							// B14字库文件UNICFxxxx.fwb不存在或缺失出错
	KCM_ERROR_AUDB = 0x00008000,							// B15音频协处理器B芯片通讯不正常
	KCM_ERROR_B_FWB = 0x00010000,							// B16音频协处理器B芯片固件不存在或缺失出错
	KCM_ERROR_DECODE = 0x00020000,							// B17解码文件Decodxxxx.fwb不存在或缺失出错
	KCM_ERROR_AC3 = 0x00040000,								// B18支持AC3解码
	KCM_ERROR_DTS = 0x00080000,								// B19支持DTS解码
	KCM_ERROR_AAC = 0x00100000,								// B20支持AAC解码
	KCM_ERROR_MP3 = 0x00200000								// B21支持MP3解码
} KC3X_ERROR_FLAG;

typedef enum {
	SPK_CH_FL = 0,											// 前置左声道
	SPK_CH_FR = 1,											// 前置右声道
	SPK_CH_CE = 2,											// 中置声道
	SPK_CH_SW = 3,											// 超低音声道
	SPK_CH_SL = 4,											// 环绕左声道
	SPK_CH_SR = 5,											// 环绕右声道
	SPK_CH_BL = 6,											// 后置左声道
	SPK_CH_BR = 7,											// 后置右声道
	SPK_CH_TL = 8,											// 顶部左声道
	SPK_CH_TR = 9											// 顶部右声道
} KC3X_SPK_CHANNEL;					
					
typedef enum {					
	FLAG_CTRL_STANDBY = 0x01,								// 待机
	FLAG_CTRL_MUTE = 0x02,									// 静音	
	FLAG_CTRL_AC3_DRC = 0x04								// 杜比数码动态压缩
} KC3X_FLAG_CTRL;											// 标志控制位
					
typedef enum {					
	KCM_MODEL_32C = 0x31,									// 模块型号KC32C
	KCM_MODEL_33A = 0x37,									// 模块型号KC33A
	KCM_MODEL_35H = 0x53,									// 模块型号KC35H
	KCM_MODEL_37H = 0x57									// 模块型号KC37H
} KC3X_MODEL_TYPE;

typedef enum {
	KCM_STATE_PLAY_PLAY = 0x01,								// 多媒体状态标志播放
	KCM_STATE_PAUSE = 0x02,									// 多媒体状态标志暂停
	KCM_STATE_STOP = 0x04,									// 多媒体状态标志停止
	KCM_STATE_PLAY_END = 0x08,								// 多媒体状态文件播放完成，已经停止
	KCM_STATE_RANDOM = 0x10,								// 多媒体远程改变了随机标志 
	KCM_STATE_REPEAT = 0x20  								// 多媒体远程改变了重复标志
} KC3X_PLAY_STATE;											// 多媒体文件播放状态
						
typedef enum {					
	KCM_OPERATE_PLAY = 0x01,								// 多媒体播放
	KCM_OPERATE_PAUSE = 0x02,								// 多媒体暂停
	KCM_OPERATE_STOP = 0x04,								// 多媒体停止
	KCM_OPERATE_FAST_FORW = 0x10,							// 多媒体播放快进
	KCM_OPERATE_FAST_BACK = 0x20							// 多媒体播放快退
} KC3X_PLAY_OPERATE;										// 多媒体文件播放控制

typedef enum {
	KCM_VOL_CHIP_NONE = 0x00,								// 不使用模块内部的音量
	KCM_VOL_CHIP_DSP_05 = 0x01,								// 使用DSP数码音量，每步0.5dB
	KCM_VOL_CHIP_DSP_10 = 0x02,								// 使用DSP数码音量，每步1dB
	KCM_VOL_CHIP_PT2258 = 0x03,								// 使用PT2258 + PT2259或者兼容的音量芯片
	KCM_VOL_CHIP_M62446 = 0x04,	 							// 使用M62446或者兼容的音量芯片
	KCM_VOL_CHIP_CS3318 = 0x05								// 使用CS3318或者兼容的音量芯片
} KC3X_VOLUME_CHIP;

typedef enum {
	CUSTOM_USB_SD_PORT = 	0x000000e0,						// 允许使用各种功能的USB及SD卡
	CUSTOM_ENA_DOWNMIX = 	0x00000100,						// 允许解码输出后的声道下混模式及各种聆听模式
	CUSTOM_ENA_BASS_MGR = 	0x00000200, 					// 允许喇叭设置及低音管理
	CUSTOM_ENA_DELAY_LIN = 	0x00000400, 					// 允许各声道延迟时间及齿音同步
	CUSTOM_ENA_VOL_TRIM = 	0x00000800, 					// 允许各声道音量及声道微调
	CUSTOM_ENA_TONE_EQ = 	0x00001000, 					// 允许音调及多段EQ均衡音效
	CUSTOM_ENA_SPECTRUM = 	0x00004000, 					// 允许电平取样及频谱输出
	CUSTOM_ENA_WIFI_BT = 	0x00008000, 					// 允许使用WIFI蓝牙网络
					
	CUSTOM_SWAP_CHANNEL = 	0x00070000,						// 互换输出声道
	CUSTOM_6CH_TO_8CH = 	0x00080000,						// 在设计为5.1的系统之中使用7.1功能
	CUSTOM_VOLUME_CHIP = 	0x00700000,						// 音量芯片类型
	CUSTOM_IN_EACH_MODE = 	0x00800000,						// 为每个输入通道单独记忆聆听模式及多段EQ均衡音效选择
	CUSTOM_MIC_LTRT_SWAP = 	0x08000000,						// 话筒MIC与模拟输入交换
	CUSTOM_MIC_ENABLE = 	0x10000000,						// 话筒声音混合功能
	CUSTOM_BCK_WCK_INPUT = 	0x20000000,						// 数码输入输出时钟输入输出选择
	CUSTOM_I2S_DATA = 		(int)0xc0000000					// 数码输入输出格式数据选择
} KC3X_CUSTOM_FLAG;


////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
typedef enum {
	KC3X_CODE_NONE = 0,					// 不支持的字符集
	KC3X_CODE_UNICODE = 1,				// 字符集为UNICODE
	KC3X_CODE_UTF_8 = 2,				// 字符集为UTF-8
	KC3X_CODE_GB2312 = 3,				// 字符集为GB2312（GBK），字库支持7614个字
	KC3X_CODE_GBK = 4,					// 字符集为GBK，往前兼容GB2312，字库支持23940个字
	KC3X_CODE_BIG5 = 5					// 字符集为BIG5
} KC3X_TEXT_CODE;

typedef enum {
	KC3X_CMD_NONE = 0,					// 没有指令，未使用的类型
	KC3X_CMD_ENABLE = 1,				// 允许使用指令 [0]输出内码[1]输入内码[2]字库Y方向像素[3]每像素的位数[4:5]显示宽[6:7]显示高
	KC3X_CMD_LOG = 2,					// 接管KCM内部的日志，只读
	KC3X_CMD_REGISTER = 3,				// 与寄存器索引值相同，只读
	KC3X_CMD_RTC_TIME = 4,				// 实时时间
	KC3X_CMD_PROGUCE_SIGNAL = 5,		// 配置模块内部产生的信号	
	
	KC3X_CMD_FILE_NAME = 6,				// 当前歌曲名改变 自动产生KCM_RD_COMMAND 
	KC3X_CMD_GET_FONT = 7,				// 通过内码获取字库
	KC3X_CMD_GET_IMAGE = 8,				// 通过文件名获取图像
	
	KC3X_CMD_SET_NETWORK = 11,			// 设置网络打开/关闭
	KC3X_CMD_IPV4 = 12,					// 网络IPV4地址改变，只读
	KC3X_CMD_IPV6 = 13,					// 网络IPV6地址改变，只读
	KC3X_CMD_WIFI_STATE = 14,			// WIFI连接状态改变
	KC3X_CMD_IPS_CONFIG = 16,			// 配置网络结构
	KC3X_CMD_WIFI_NAME = 17,			// 连接的WIFI名
	KC3X_CMD_WIFI_PSW = 18,				// 连接的WIFI密码
	KC3X_CMD_HOT_NAME = 19,				// 热点WIFI
	KC3X_CMD_HOT_PSW = 20,				// 热点密码
	KC3X_CMD_ACCOUNT = 21,				// 账号
	KC3X_CMD_ACC_PSW = 22,				// 密码
	KC3X_CMD_DNS_NAME = 23,				// 域名名
	KC3X_CMD_DNS_PSW = 24,				// 域名密码
	KC3X_CMD_DNS_VISIT = 25,			// 域名访问码
	KC3X_CMD_NICK_NAME = 26,			// 昵称
	KC3X_CMD_BRAND = 27,				// 牌子
	KC3X_CMD_MODEL = 28,				// 型号
	KC3X_CMD_MANUFACTURER = 29,			// 生产厂商	
	
	KC3X_CMD_HTTP_PLAY = 33				// 播放指定链接的文件
	
} KC3X_COMMAND_TYPE;					// 指令类型 KCM_WR_COMMAND KCM_RD_COMMAND

typedef struct {
	unsigned char LogLevel : 4;			// 接管KCM内部的日志
	unsigned char AutoFlag : 4;			// 自动产生KCM_RD_COMMAND的标志 1当前歌曲名
	unsigned char OutCode : 4;			// 输出的内码 KC3X_TEXT_CODE
	unsigned char InCode : 4;			// 输入的内码 KC3X_TEXT_CODE
	unsigned char FontSize;				// 字库Y方向像素
	unsigned char Bits;					// 显示每像素的位数
	unsigned short Width;				// 显示的宽
	unsigned short Height;				// 显示的高
} KC3X_CMD_ENA_SET;						// KC3X_CMD_ENABLE 允许使用指令


typedef struct {
	unsigned int Ipv4;					// IP地址
	unsigned int Mask;					// 掩码
	unsigned int Gateway;				// 网关	
	unsigned int Dns1;					// DNS服务器地址1	
	unsigned int Dns2;					// DNS服务器地址2
} KC3X_NET_CONFIG;						// 网络配置结构



#endif		// __KC3X_TYPE_H


